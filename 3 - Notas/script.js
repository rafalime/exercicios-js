let notas = []

for (let i = 1; i < 4; i++){
    notas.push(parseFloat(prompt("Digite a nota "+i)))
}

let media = 0
if (notas.length != 0){
    for (let nota of notas){
        media = media + nota
    }
    media = media / notas.length
    media = media.toFixed(2)

    if (media >= 6){
        alert("Aprovado, média: " + media)
    }else{
        alert("Reprovado, média: " + media)
    }
}
