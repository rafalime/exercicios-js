function getMsg(num) {
    if ((num % 3 == 0) && (num % 5 == 0)) {
        return "fizzbuzz"
    }
    if (num % 3 == 0){
        return "fizz"
    }
    if (num % 5 == 0){
        return "buzz"
    }
    return ""
}

for (let i = 0; i < 5; i++){
    let msg = getMsg(parseInt(prompt("Digite o número " + (i+1))))
    console.log(msg)
}
